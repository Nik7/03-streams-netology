"use strict";
// лекция 1:18
// про Tranform streams
// http://codewinds.com/blog/2013-08-20-nodejs-transform-streams.html#what_are_transform_streams_


const fs = require('fs');
const cryptoMd5Hash = require('crypto').createHash('MD5');
const inputFileStream = fs.createReadStream('input.txt');
const outputFileStream = fs.createWriteStream('output.txt');
const streamTransform = require('stream').Transform;

class transformStream extends streamTransform{
 constructor(options){
   super(options);	
 }
 
  _transform(chunk, encoding, callback){
    this.push(chunk.toString('hex'));
    callback(); 
 }
}; 

const tr = new transformStream();


const rs = inputFileStream.pipe(cryptoMd5Hash); 
rs.pipe(outputFileStream);
rs.pipe(process.stdout);   
rs.pipe(tr).pipe(process.stdout);



