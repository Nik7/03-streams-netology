"use strict";

const fs = require('fs');
const cryptoMd5Hash = require('crypto').createHash('MD5');
const inputFileStream = fs.createReadStream('input.txt');
const outputFileStream = fs.createWriteStream('output.txt');

const rs = inputFileStream.pipe(cryptoMd5Hash); 

rs.pipe(outputFileStream);
rs.pipe(process.stdout);