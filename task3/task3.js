"use strict";

const {Readable, Writable, Transform}  = require("stream");

//======================================================
class CReadable extends Readable {
  constructor(options) {
    super(options);
  }

  _read(size) {
    
     let i = Math.floor(Math.random() * 10);
    
     this.push(i.toString());	
    

  }
}

//======================================================
class CWritable extends Writable {
  constructor(options) {
    super(options);
  }

  _write(chunk, encoding, done) {
    process.stdout.write(chunk.toString());
    done();
  }
}


//======================================================

class CTransform extends Transform {
  constructor(options) {
    super(options);
  }

  _transform(chunk, encoding, callback) {
    setTimeout(()=>{
    	let m = Number(chunk)*100;
        this.push(m.toString());
        callback();
    }, 1000);
  }
}
//======================================================

const wr = new CWritable();
const rs = new CReadable();
const tf = new CTransform();

rs.pipe(tf).pipe(wr);	